var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , home = require('./routes/home')
  , device = require('./routes/device')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3300);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

app.post('/signin', home.afterSignIn);


app.get('/devicePage', device.devicePage);
app.get('/cloudBrokerPage', device.cloudBrokerPage);
app.post('/cloudBroker', device.changeCloudBroker);

app.get('/loadDevices', device.loadDevices);

app.post('/device', device.addDevice);
app.post('/deleteDevice', device.deleteDevice);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
