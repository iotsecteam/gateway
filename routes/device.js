var ejs = require("ejs");
var mysql = require('./mysql');

exports.devicePage = function(req, res) {
	res.render('device');
};

exports.changeCloudBroker = function(req,res)
{
	var brokerUrl = req.param("brokerUrl");
	console.log("broker is "+req.param("brokerUrl"));
	const readline = require('readline');
	const fs = require('fs');
	
	// --------find line number to insert new broker url------
	const rl = readline.createInterface({
	  input: fs.createReadStream('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf')
	});
	
	var lineNo = 0;
	
	rl.on('line', (line) => {
		  console.log('Line from file:', line);
		  var match = /address/.exec(line);
		  
		  if (match) {
			console.log("match found at " +lineNo);
			var data = fs.readFileSync('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf').toString().split("\n");
			var newBroker = "address "+req.param("brokerUrl");	
			data.splice(lineNo, 1, newBroker);
			var text = data.join("\n");

			fs.writeFile('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf', text, function (err) {
			  if (err) return console.log(err);
			 
			  /*var cmd=require('node-cmd');
				
				var sys = require('util')
				var exec = require('child_process').exec;
				var child;
				
				child = exec("/usr/local/Cellar/mosquitto/1.4.8/bin/mosquitto_passwd -b /usr/local/Cellar/mosquitto/1.4.8/bin/passwordfile "+req.param("deviceId")+" "+req.param("password"), function (error, stdout, stderr) {
				  console.log('stdout: ' + stdout);
				  console.log('stderr: ' + stderr);
				  if (error !== null) {
				    console.log('exec error: ' + error);
				  }
					child = exec("launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
						  console.log('stdout: ' + stdout);
						  console.log('stderr: ' + stderr);
						  
						  child = exec("launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
							  console.log('stdout: ' + stdout);
							  console.log('stderr: ' + stderr);
							
							  
							  res.render('cloudBroker', {brokerurl:JSON.stringify(brokerUrl)});  
							    return;		  
					});

				});
					
				});	*/
		
			});
			  
			  
			  
		 
		  }
		  else
		  {
		  lineNo++;
		  }
	});
	res.end();
}

exports.cloudBrokerPage = function(req, res) {
	const readline = require('readline');
	const fs = require('fs');
	
	// --------find line number to insert new topic on for new device------
	const rl = readline.createInterface({
	  input: fs.createReadStream('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf')
	});
	var lineNo = 1;
	var brokerUrl = "";

	rl.on('line', (line) => {
		  console.log('Line from file:', line);
		  var match = /address/.exec(line);
		  
		  if (match) {
		      //console.log("cloudBroker found at " + lineNo);
		      brokerUrl = line.substring(8);
		      console.log("brokerUrl"+brokerUrl);
		      res.render('cloudBroker', {brokerurl:JSON.stringify(brokerUrl)});  
		    return; 
		  }
	});
	
	//res.render('cloudBroker', {brokerurl:''});
};

exports.loadDevices = function(req,res) {
	var getDevices="select * from devices";
	
	mysql.fetchData(function(err,deviceResults){
		if(err){
			throw err;
			}
		else 
		{
			res.send({"deviceResults":JSON.stringify(deviceResults)});
		}
	},getDevices);
};
	

exports.deleteDevice = function(req,res) {
	const readline = require('readline');
	const fs = require('fs');

	var deviceIdToDelete =req.param("deviceIdToDelete"); 
	
	// --------find line number to insert new topic on for new device------
	const rl = readline.createInterface({
	  input: fs.createReadStream('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf')
	});
	var lineNo = 0;

	rl.on('line', (line) => {
		  console.log('Line from file:', line);
		  var patt = new RegExp(deviceIdToDelete);
		  var match = patt.exec(line);
		  if (match) {
		      console.log("Delete line number " + lineNo);
		     
		
			var data = fs.readFileSync('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf').toString().split("\n");
			data.splice(lineNo, 1);
				var text = data.join("\n");

				fs.writeFile('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf', text, function (err) {
				  if (err) return console.log(err);
				});
		  }
		  else
			  {
			  lineNo++;
			  }
		 
			});
	
	var cmd=require('node-cmd');
	
	var sys = require('util')
	var exec = require('child_process').exec;
	var child;
	
	child = exec("/usr/local/Cellar/mosquitto/1.4.8/bin/mosquitto_passwd -D /usr/local/Cellar/mosquitto/1.4.8/bin/passwordfile "+req.param("deviceId")+" "+req.param("pwd"), function (error, stdout, stderr) {
	  console.log('stdout: ' + stdout);
	  console.log('stderr: ' + stderr);
	  if (error !== null) {
	    console.log('exec error: ' + error);
	  }
		child = exec("launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
			  console.log('stdout: ' + stdout);
			  console.log('stderr: ' + stderr);
			  
			  child = exec("launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
				  console.log('stdout: ' + stdout);
				  console.log('stderr: ' + stderr);
		});

	});
		
	});
	

	var query = "delete from devices where iddevices = '"+req.param("deviceIdToDelete")+"'";
	console.log("Query is:"+query);

	
	mysql.fetchData(function(err,results){
		if(err){
			throw err;
			}
		else 
		{
			var getNewDevice="select * from devices";
			console.log("Devices Query is:"+getNewDevice);
			
			mysql.fetchData(function(err,deviceResults) {

				if (!err) {
					console.log("Device deleted successfully");	
					res.send({"modifiedDevicesResults": JSON.stringify(deviceResults) });	
						//res.end(result);
					}
					// render or error
					else {
						res.end('An error occurred');
						console.log("error cause of duplication"+err);
					}
				}, getNewDevice);
			}
		 
	},query);
}


exports.addDevice = function(req,res) {
	const readline = require('readline');
	const fs = require('fs');

	console.log(req.param("deviceId"));
	console.log(req.param("pwd"));
	console.log(req.param("dtype"));
	console.log(req.param("status"));
	
	
	
	//Add device in DB
	var today = new Date();
	var dd = today.getDate();
    var mm = today.getMonth()+1; 

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = mm+'/'+dd+'/'+yyyy;
	var strDate = today.toString();
	var query = "insert into devices values('"+req.param("deviceId")+"','"+req.param("pwd")+"','"+req.param("dtype")+"','"+req.param("status")+"','"+strDate+"', '')";
	console.log("Query is:"+query);

	
	mysql.fetchData(function(err,results){
		if(err){
			console.log(err);
			throw err;
			}
		else 
		{
			
			// --------find line number to insert new topic on for new device------
			const rl = readline.createInterface({
			  input: fs.createReadStream('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf')
			});
			var lineNo = 1;

			rl.on('line', (line) => {
				  console.log('Line from file:', line);
				  var match = /address/.exec(line);
				  if (match) {
				      console.log("match found at " + lineNo);
				      lineNo++;
						console.log("line number to insert"+lineNo);
				
					var data = fs.readFileSync('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf').toString().split("\n");
					var newTopic = "topic # out 2 iotSecurity/"+req.param("deviceId")+"/";	
					data.splice(lineNo, 0, newTopic);
						var text = data.join("\n");

						fs.writeFile('/usr/local/Cellar/mosquitto/1.4.8/etc/mosquitto/mosquitto.conf', text, function (err) {
						  if (err) return console.log(err);
						});
				  }
				  else
					  {
					  lineNo++;
					  }
				 
					});
			
			
			var cmd=require('node-cmd');
			var sys = require('util')
			var exec = require('child_process').exec;
			var child;
			
			child = exec("/usr/local/Cellar/mosquitto/1.4.8/bin/mosquitto_passwd -b /usr/local/Cellar/mosquitto/1.4.8/bin/passwordfile "+req.param("deviceId")+" "+req.param("pwd"), function (error, stdout, stderr) {
			  console.log('stdout: ' + stdout);
			  console.log('stderr: ' + stderr);
			  if (error !== null) {
			    console.log('exec error: ' + error);
			  }
				child = exec("launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
					  console.log('stdout: ' + stdout);
					  console.log('stderr: ' + stderr);
					  
					  child = exec("launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist", function (error, stdout, stderr) {
						  console.log('stdout: ' + stdout);
						  console.log('stderr: ' + stderr);
				});

			});
				
			});
			var getNewDevicee="select * from devices";
			console.log("Devices Query is:"+getNewDevicee);
			
			mysql.fetchData(function(err,deviceResults) {

				if (!err) {
					console.log("Device added successfully");	
					res.send({"addedDeviceResults": JSON.stringify(deviceResults) });	
						//res.end(result);
					}
					// render or error
					else {
						res.send({"addedDeviceResults": null });
						console.log(err);
					}
				}, getNewDevicee);
			}
		 
	},query);
	
	
	
	
	
	
	
}


