var ejs= require('ejs');
var mysql = require('mysql');

function getConnection(){
	var connection = mysql.createConnection({
	    host     : 'localhost',
	    user     : 'iot',
	    password : '',
	    database : 'iotgateway'
	});
	return connection;
}


function fetchData(callback,sqlQuery){
	
	console.log("\nSQL Query::"+sqlQuery);
	
	var connection=getConnection();
	
	connection.query(sqlQuery, function(err, rows, fields) {
		if(err){
			console.log("ERROR: " + err.message);
		}
		else 
		{	// return err or result
			console.log("DB Results:"+rows);
			callback(err, rows);
		}
	});
	console.log("\nConnection closed..");
	connection.end();
}	

exports.fetchData=fetchData;

/*var fs = require("fs");
var file = "iotgateway.db";
var exists = fs.existsSync(file);

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);


function insertData(){
console.log("1");
	    db.run("CREATE TABLE IF NOT EXISTS devices(device_id string PRIMARY KEY), password string, device_type string, status string, date_added string, date deleted string)");
	    console.log("2");	  	  
	  
	   var stmt = db.prepare("INSERT INTO devices VALUES (?,?,?,?,?,?)");
	  stmt.run("dev2", "123", "Temprature sensor", "active", "03/19/2016","-", 	function(err, row){
		  if(err)
			  {
			  console.log("8");
				console.log("ERROR: " + err.message);
			  }
		  else {
			  stmt.finalize();
			  console.log("3");
			  console.log("4");
		  }
	  });
	  
	}


function fetchData(callback,sqlQuery){
	console.log("5");
	db.each("SELECT * FROM devices", function(err, row) {
		  
		  if(err){
			  console.log("6");
				console.log("ERROR: " + err.message);
			}
			else 
			{	// return err or result
				 console.log("row.id"+row.device_id);
				 console.log("7");
				 callback(err, row);
			}
		 
		  });
}



exports.fetchData=fetchData;
exports.insertData=insertData;*/
